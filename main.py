import telebot
from telebot import types
import requests
import json
from random import randint

bot = telebot.TeleBot('6071553689:AAEnZA5_dJU6OfGo7bK_dul_WKGmrgy6yCs')
API = 'b3677a0fba1c70bb74f6190b9fcdf3e7'


@bot.message_handler(commands=['start'])
def start(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
    markup.add("/help")
    mess = f'Привет, <em>{message.from_user.first_name}</em>!\nМеня зовут Виталя и я реальный чел😇\nЯ могу стать твоим персональным другом!\nВместе со мной ты сможешь скоротать время❤'
    bot.send_message(message.chat.id, mess, parse_mode='html', reply_markup=markup)
    stik = open('hi.tgs', 'rb')
    bot.send_sticker(message.chat.id, stik)


@bot.message_handler(commands=['help'])
def help(message):
    markup2 = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
    markup2.add("/game", "анекдот😂", "кто я?🤨", "/weather")
    bot.send_message(message.chat.id,'<em>Я умею делать команды, которые представлены ниже.</em>\n-------\nПо команде /weather я скажу вам погоду в вашем городе и подскажу, что можно надеть\n-------\nЕсли вам грустно попросите меня рассказать вам анекдот!\n-------\nЕсли вам скучно поспользуйтесь командой /game и мы сможем с вами поиграть в цу-е-фа\n-------\nТак же вы можете узнать кто вы сегодня\n-------\nС большой любовью от команды LilChill', parse_mode='html', reply_markup=markup2)
    photo = open('love.tgs', 'rb')
    bot.send_sticker(message.chat.id, photo)


@bot.message_handler(commands=['weather'])
def get_weather(message):
    city = bot.send_message(message.chat.id, 'Введите город')
    bot.register_next_step_handler(city, weather)


def weather(message):
    city = message.text
    res = requests.get(f'https://api.openweathermap.org/data/2.5/weather?q={city}&appid={API}&units=metric')
    if res.status_code == 200:
        data = json.loads(res.text)
        feels = data["main"]["feels_like"]
        bot.reply_to(message, f'В городе {city} сейчас:'
                              f'\nТемпература - {data["main"]["temp"]}°C'
                              f'\nОщущается как - {data["main"]["feels_like"]}°C'
                              f'\nСкорость ветра - сила {data["wind"]["speed"]}'
                              f'\nВлажность - {data["main"]["humidity"]}%')
        if feels > 10.0 and feels < 15.0 :
            bot.send_message(message.chat.id, "Я бы посоветовал вам надеть ветровку или легкую куртку", parse_mode='html')
        elif feels > 15.0 and feels < 20.0:
            bot.send_message(message.chat.id, "Я бы посоветовал вам надеть ветровку, но если вы горячая штучка, то вам будет хорошо и просто в кофте", parse_mode='html')
        elif feels > 20.0 and feels < 25.0:
            bot.send_message(message.chat.id, "Я бы посоветовал вам надеть футболку, при желании можно пойти и в шортах", parse_mode='html')
        elif feels > 25.0 and feels < 30.0:
            bot.send_message(message.chat.id, "Ну и жара...\n Однозначно рекомендую вам надеть шорты и легкую футболку из дышащих материалов\nНе забывайте про головной убор!", parse_mode='html')
        elif feels > 30.0 and feels < 40.0:
            bot.send_message(message.chat.id, "А вы живы там?\nЛучше не выходить на улицу и пить больше воды ", parse_mode='html')
        elif feels > 5.0 and feels < 10.0:
            bot.send_message(message.chat.id, "Я бы посоветовал вам надеть ветровку или легкую куртку", parse_mode='html')
        elif feels > 0.0 and feels < 5.0:
            bot.send_message(message.chat.id, "Я бы посоветовал вам надеть демисезонную куртку или пальто", parse_mode='html')
        elif feels > -5.0 and feels < 0.0:
            bot.send_message(message.chat.id, "Я бы посоветовал вам надеть зимнюю куртку, желательно шапку и шарф!", parse_mode='html')
        elif feels > -10.0 and feels < -5.0:
            bot.send_message(message.chat.id, "Я бы посоветовал вам надеть зимнюю куртку, обазательно шапку и шарф!\nНу и конечно же зимнии сапоги", parse_mode='html')
        elif feels > -15.0 and feels < -10.0:
            bot.send_message(message.chat.id, "Я бы посоветовал вам надеть теплую зимнюю куртку, обазательно шапку и шарф!\nНу и конечно же зимнии сапоги с теплым носочком", parse_mode='html')
        elif feels > -20.0 and feels < -15.0:
            bot.send_message(message.chat.id, "Я бы посоветовал вам не выходить на улицу, но если выбора нет...\nНадевайте теплую зимнюю куртку, обазательно шапку и шарф!\nНу и конечно же зимнии сапоги с теплым носочком", parse_mode='html')


@bot.message_handler(commands=['game'])
def game(message):
    markup2 = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
    markup2.add("цу-е-фа")
    bot.send_message(message.chat.id,'<em>Выберите игру</em>', parse_mode='html', reply_markup=markup2)



@bot.message_handler(content_types=['text'])
def get_user_text(message):
    if message.text == "кто я?🤨":
        a = randint(1, 30)
        if a == 1:
            bot.send_message(message.chat.id, "вы - мстительный депутат", parse_mode='html')
        elif a == 2:
            bot.send_message(message.chat.id, "вы - запертый кот", parse_mode='html')
        elif a == 3:
            bot.send_message(message.chat.id, "вы- искусственная маршрутка", parse_mode='html')
        elif a == 4:
            bot.send_message(message.chat.id, "вы - летящий хлебушек", parse_mode='html')
        elif a == 5:
            bot.send_message(message.chat.id, "вы - праведная сосиска", parse_mode='html')
        elif a == 6:
            bot.send_message(message.chat.id, "вы - сбежавший из пихушки энергетик", parse_mode='html')
        elif a == 7:
            bot.send_message(message.chat.id, "вы - чумная шаурма", parse_mode='html')
        elif a == 8:
            bot.send_message(message.chat.id, "вы - веселая сосичка в тесте", parse_mode='html')
        elif a == 9:
            bot.send_message(message.chat.id, "вы - кислотный турист", parse_mode='html')
        elif a == 10:
            bot.send_message(message.chat.id, "вы - вселяющий страх вкусный мемчик", parse_mode='html')
        elif a == 11:
            bot.send_message(message.chat.id, "вы - загадочный бомж", parse_mode='html')
        elif a == 12:
            bot.send_message(message.chat.id, "вы - великий двигатель прогресса", parse_mode='html')
        elif a == 13:
            bot.send_message(message.chat.id, "вы - теплая табуретка", parse_mode='html')
        elif a == 14:
            bot.send_message(message.chat.id, "вы - легендарное такси", parse_mode='html')
        elif a == 15:
            bot.send_message(message.chat.id, "вы - коварный щенок", parse_mode='html')
        elif a == 16:
            bot.send_message(message.chat.id, "вы - пришедшая из параллельной вселенной шевелящаяся бочка", parse_mode='html')
        elif a == 17:
            bot.send_message(message.chat.id, "вы - вечно скитающийся коварный разум", parse_mode='html')
        elif a == 18:
            bot.send_message(message.chat.id, "вы - новый огурец", parse_mode='html')
        elif a == 19:
            bot.send_message(message.chat.id, "вы - быстрорастворимый демон", parse_mode='html')
        elif a == 20:
            bot.send_message(message.chat.id, "вы - безнадежный рандом", parse_mode='html')
        elif a == 21:
            bot.send_message(message.chat.id, "вы - старый волк", parse_mode='html')
        elif a == 22:
            bot.send_message(message.chat.id, "вы - мудрая тигрица", parse_mode='html')
        elif a == 23:
            bot.send_message(message.chat.id, "вы - гордый чайник", parse_mode='html')
        elif a == 24:
            bot.send_message(message.chat.id, "вы - рекомендуемый кусок кода", parse_mode='html')
        elif a == 25:
            bot.send_message(message.chat.id, "вы - брутальная курица", parse_mode='html')
        elif a == 26:
            bot.send_message(message.chat.id, "вы - жареный дед", parse_mode='html')
        elif a == 27:
            bot.send_message(message.chat.id, "вы - хитрый пингвин", parse_mode='html')
        elif a == 28:
            bot.send_message(message.chat.id, "вы - опасный император", parse_mode='html')
        elif a == 29:
            bot.send_message(message.chat.id, "вы - тайная микроволновка", parse_mode='html')
        elif a == 30:
            bot.send_message(message.chat.id, "вы - бруталььный паук", parse_mode='html')
    elif message.text == "анекдот😂":
        b = randint(1, 30)
        if b == 1:
            bot.send_message(message.chat.id, "Армянская битва экстрасенсов \n- Перед вами 20 машин, в одной из них ваш брат... \n- Азорчик!\n- Оу \n- Он там", parse_mode='html')
        elif b == 2:
            bot.send_message(message.chat.id, "Пацаны покупают сигареты возле ларька, один купил пачку, смотрит на неё и говорит:\n -у меня рак, а у тебя че?\n -а у меня импотенция \n-че грустный такой? \n-А я не курю просто", parse_mode='html')
        elif b == 3:
            bot.send_message(message.chat.id, "Парень приводит девушку домой, знакомиться. Та заходит в коридор, наклоняется туфли снять и громко пердит, смущается и смотря в потолок говорит: - ой, какая у Вас люстра красивая... Ей парень отвечает: - в зал зайдешь вообще обосрёшься!", parse_mode='html')
        elif b == 4:
            bot.send_message(message.chat.id, "Начальник тюрьмы обращается к смертнику, сидящему на электрическом стуле:\n — Ваше последнее желание? \n— Пожалуйста, держите меня за руку. Мне так будет спокойнее.", parse_mode='html')
        elif b == 5:
            bot.send_message(message.chat.id, "анекдот про колобка АХхазаха \nПовели как-то раз колобка на расстрел, подводят к стене и говорят: давай своё последнее желание. \nКолобок отвечает: блин только в голову не стреляйте...", parse_mode='html')
        elif b == 6:
            bot.send_message(message.chat.id, "Сидят мужики и пьют пиво. Потом один из них достает из кармана мааааленького мужичка, ставит его на стол, наливает ему в наперсток пива и говорит:\n — Ну, Петрович, расскажи мужикам, как ты в Африке колдуна нафиг послал...", parse_mode='html')
        elif b == 7:
            bot.send_message(message.chat.id, "1960-70 е годы. СССР. Урок патриотизма в детском саду. Воспитательница спрашивает детей:\n-Где у детей самые хорошие книжки? Самые добрые сказки?\nДети, хором:\n-В Советском Союзе!\n-Где у детей самое вкусные конфеты? Самые большие яблоки?\n-В Советском Союзе!\n-Где у детей самые ласковые воспитатели?\n-В Советском Союзе!\n-Где у детей самые красивые игрушки?\n-В Советском Союзе!\n-Вовочка, что ты плачешь?\n-Хочу жить в Советском Союзе!", parse_mode='html')
        elif b == 8:
            bot.send_message(message.chat.id, "Лекция в мединституте. Профессор выходит к аудитории и говорит: - Перед началом лекции я расскажу вам одну историю. В молодости у меня был друг. Нам с ним нравилась одна и та же девушка. Мы оба хотели на ней жениться. Она выбрала его: я остался с носом, а мой друг - без... Итак, тема нашей сегодняшней лекции - сифилис и его последствия.", parse_mode='html')
        elif b == 9:
            bot.send_message(message.chat.id, "Токарь третьего разряда Сидоров не женился, потому что на заводе высоко ценили отсутствие брака.", parse_mode='html')
        elif b == 10:
            bot.send_message(message.chat.id, "Мальчик раскачивался на табуретке, упал и сломал все шесть ножек.", parse_mode='html')
        elif b == 11:
            bot.send_message(message.chat.id, "Умирает старый еврей, на последнем вздохе:\n- Где моя жена?\n- Я рядом, милый!\n- Где мои дети?\n- Мы здесь, папа!\n- А внуки мои тоже здесь?\n- Здесь, дедушка!\n- А свет на кухне почему горит?!", parse_mode='html')
        elif b == 12:
            bot.send_message(message.chat.id, "Бабка, заметившая в маршрутке свободное сидение, выпрыгнула из такси..", parse_mode='html')
        elif b == 13:
            bot.send_message(message.chat.id, "Никита Хрущев радостно принимает первомайскую демонстрацию на Красной площади. Спустившись с мавзолея, здоровается с трудящимися. Тут к нему подбегает маленькая девочка и говорит:\n- Никита Сергеевич, мой папа говорит, что растрачивая народные деньги на покорение космоса, вы запустили сельское хозяйство.\nХрущев наклонился к девочке и говорит:\n- Передай своему папе, что я умею сажать не только кукурузу.", parse_mode='html')
        elif b == 14:
            bot.send_message(message.chat.id, "- Стой! Что ты пьешь! Это же метанол, ты что, не видишь?!\b- Нет", parse_mode='html')
        elif b == 15:
            bot.send_message(message.chat.id, "В ресторане:\n- что вам?\n- пюре и свинину…\n- лучком посыпать?\n- а это бесплатно?\n- а Вам точно свинину можно?", parse_mode='html')
        elif b == 16:
            bot.send_message(message.chat.id, "Запрыгивает еврей в такси и кричит:\n-газу давай!",parse_mode='html')
        elif b == 17:
            bot.send_message(message.chat.id, "Письмо из центра до Штирлица не дошло. Штирлиц прочитал еще раз. Все равно не дошло.", parse_mode='html')
        elif b == 18:
            bot.send_message(message.chat.id, "Как называют араба в гробу?\n.\nБумбокс", parse_mode='html')
        elif b == 19:
            bot.send_message(message.chat.id, "С небоскреба одновременно падают негр и цыган. Кто упадет первым?\n.\n.\n.\n.\n.\n.\nУровень преступности", parse_mode='html')
        elif b == 20:
            bot.send_message(message.chat.id, "Армянин решил баллотироваться в горсовет. В регистратуре:\n- Род деятельности?\n- Машинист\n- Возраст?\n- 35 лет\n- Национальность?\n- Армянин\n- Партия?\n- Давайте", parse_mode='html')
        elif b == 21:
            bot.send_message(message.chat.id, "Трое неизвестных забрали у прохожего паспорт и порвали. Теперь неизвестных четверо.", parse_mode='html')
        elif b == 22:
            bot.send_message(message.chat.id, "В дурке шизики рыбачат в унитазе. Подходит к ним врач и говорит:\n -клюёт? \nВсе говорят :-да, один только говорит :-нет-. Врач лумает:-идёт, значит, на поправку.\nПотом его спрашивают остальные шизы:-а что ты сказал, что не клюёт?\n он отвечает :-аа что я буду рыбные места выдавать", parse_mode='html')
        elif b == 23:
            bot.send_message(message.chat.id, "Чем отличается десантник от сапера? — направлением полета.", parse_mode='html')
        elif b == 24:
            bot.send_message(message.chat.id, "Армянин нетрадиционной ориентации играет в шашки.", parse_mode='html')
        elif b == 25:
            bot.send_message(message.chat.id, "Купили два грузина лошадь. Привели её домой, осмотрели и заметили что она худая, мешок костей. Решили вставить ей в задницу трубку и надуть. Один дует–дует, дует–дует, устал:\n— Гиви, я усталь, твоя очеред!\nВторой достает из задницы трубку и вставляет другим концом:\n— Гиви, ты щито делаешь?!\n— Я брэзгую!", parse_mode='html')
        elif b == 26:
            bot.send_message(message.chat.id, "Идёт мужчина на работу и видит по пути маленькую такую бабушку. Она вся в морщинках, старенькая, сгорбившаяся, скромно стоит в платочке и продает редис. Маленькие пучочки, а на них кусочек картонки, на которой от руки аккуратненько написано: 100 рублей\nНу мужик сжалился: даёт ей сотню, а сам редис не берёт. С работы идёт назад— та же история.\nИ ходил он так месяц. На работу идёт— сто рублей даст, с работы идёт— даст сто рублей.\nИ вот, даёт он этой бабушке в очередной раз сто рублей, а она его хвать за руку.\nОн, улыбаясь:\n—Вам, наверное, интересно, почему я каждый раз деньги даю, а редис не беру?)\nБабушка в ответ:\n—Да мне пофиг. С сегодняшнего дня редис 150 рублей стоит", parse_mode='html')
        elif b == 27:
            bot.send_message(message.chat.id, "Доктор в больнице:\n- Поскольку Вы ко мне на приём пришли впервые, то, наверное было бы лучше, чтоб вы рассказали всё с самого начала.\n- Конечно, без проблем. Итак, вначале Бог сотворил небо и землю...", parse_mode='html')
        elif b == 28:
            bot.send_message(message.chat.id, "Моя девушка попросила меня сбрить бороду, потому что колется...я сбрил, но она не перестала колоться и умерла", parse_mode='html')
        elif b == 29:
            bot.send_message(message.chat.id, "Прапорщик смотрит на перевернутую вверх дном кружку:\n-Ну что за фигня: верх - запаян, дно-отсутствует.", parse_mode='html')
        elif b == 30:
            bot.send_message(message.chat.id, "Роскосмос. И вырос.", parse_mode='html')
    elif message.text == "цу-е-фа":
        markup4 = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=3)
        markup4.add("камень 👊", "ножницы ✌", "бумага ✋")
        a = bot.send_message(message.chat.id, 'Камень, ножницы, бумага\n цу-е-фа!!!', parse_mode='html', reply_markup=markup4)
        bot.register_next_step_handler(a, kmn)
    elif message.text == "да!":
        markup4 = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=3)
        markup4.add("камень 👊", "ножницы ✌", "бумага ✋")
        a = bot.send_message(message.chat.id, 'Камень, ножницы, бумага\n цу-е-фа!!!', parse_mode='html',reply_markup=markup4)
        bot.register_next_step_handler(a, kmn)
    elif message.text == "не хочу":
        markup4 = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
        markup4.add("/game", "анекдот😂", "кто я?🤨", "/weather")
        bot.send_message(message.chat.id, 'Как скажете!', parse_mode='html',reply_markup=markup4)

    else:
        bot.send_message(message.chat.id, "Котики изучают вопросики 🫢\nК сожалению, я вас не понимаю 😔\nМои команды вы можете узнать через команду /help!!", parse_mode='html')
        stic = open('error.tgs', 'rb')
        bot.send_sticker(message.chat.id, stic)



def kmn(message):
    if message.text == "камень 👊":
        c = randint(1, 3)
        if c == 1:
            bot.send_message(message.chat.id, "камень 👊", parse_mode='html')
            bot.send_message(message.chat.id, "ничья!", parse_mode='html')
            markup5 = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
            markup5.add("да!", "не хочу")
            bot.send_message(message.chat.id, 'поиграем еще?', parse_mode='html', reply_markup=markup5)

        elif c == 2:
            bot.send_message(message.chat.id, "ножницы ✌", parse_mode='html')
            bot.send_message(message.chat.id, "вы победили!!", parse_mode='html')
            markup6 = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
            markup6.add("да!", "не хочу")
            bot.send_message(message.chat.id, 'поиграем еще?', parse_mode='html', reply_markup=markup6)
        elif c == 3:
            bot.send_message(message.chat.id, "бумага ✋", parse_mode='html')
            bot.send_message(message.chat.id, "в этот раз победа за мной))", parse_mode='html')
            markup7 = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
            markup7.add("да!", "не хочу")
            bot.send_message(message.chat.id, 'поиграем еще?', parse_mode='html', reply_markup=markup7)
    elif message.text == "ножницы ✌":
        d = randint(1, 3)
        if d == 1:
            bot.send_message(message.chat.id, "камень 👊", parse_mode='html')
            bot.send_message(message.chat.id, "в этот раз победа за мной))", parse_mode='html')
            markup8 = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
            markup8.add("да!", "не хочу")
            bot.send_message(message.chat.id, 'поиграем еще?', parse_mode='html', reply_markup=markup8)
        elif d == 2:
            bot.send_message(message.chat.id, "ножницы ✌", parse_mode='html')
            bot.send_message(message.chat.id, "ничья!", parse_mode='html')
            markup9 = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
            markup9.add("да!", "не хочу")
            bot.send_message(message.chat.id, 'поиграем еще?', parse_mode='html', reply_markup=markup9)
        elif d == 3:
            bot.send_message(message.chat.id, "бумага ✋", parse_mode='html')
            bot.send_message(message.chat.id, "вы победили!!", parse_mode='html')
            markup10 = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
            markup10.add("да!", "не хочу")
            bot.send_message(message.chat.id, 'поиграем еще?', parse_mode='html', reply_markup=markup10)
    elif message.text == "бумага ✋":
        i = randint(1, 3)
        if i == 1:
            bot.send_message(message.chat.id, "камень 👊", parse_mode='html')
            bot.send_message(message.chat.id, "вы победили!!", parse_mode='html')
            markup11 = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
            markup11.add("да!", "не хочу")
            bot.send_message(message.chat.id, 'поиграем еще?', parse_mode='html', reply_markup=markup11)
        elif i == 2:
            bot.send_message(message.chat.id, "ножницы ✌", parse_mode='html')
            bot.send_message(message.chat.id, "в этот раз победа за мной))", parse_mode='html')
            markup12 = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
            markup12.add("да!", "не хочу")
            bot.send_message(message.chat.id, 'поиграем еще?', parse_mode='html', reply_markup=markup12)
        elif i == 3:
            bot.send_message(message.chat.id, "бумага ✋", parse_mode='html')
            bot.send_message(message.chat.id, "ничья!", parse_mode='html')
            markup13 = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
            markup13.add("да!", "не хочу")
            bot.send_message(message.chat.id, 'поиграем еще?', parse_mode='html', reply_markup=markup13)

bot.polling(none_stop=True)